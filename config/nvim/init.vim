
if filereadable($HOME."/.config/nvim/plugin.vim")
  source ~/.config/nvim/plugin.vim 
endif
" show line numbers
set number
" mode is displayed on lightline
set noshowmode 
" Disable Arrow keys in Escape mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Disable Arrow keys in Insert mode
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>"

"Easier split navigation 
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" save with ctrl + c

:imap <c-s> <Esc>:w<CR>a

" custom exit for terminal nvim
tnoremap <Esc> <C-\><C-n>
" map ESC to jk 
inoremap jk <ESC> 
" Disable bell
set visualbell

" Theme 
syntax on 
if filereadable($HOME . "/.config/nvim/init.vim.local")
  source ~/.config/nvim/init.vim.local
endif
