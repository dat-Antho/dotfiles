" Install plugin manager if not installed yet
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
	silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"Custom mapping
map <C-n> :NERDTreeToggle<CR>
map ; :Files<CR>
" Plugins
call plug#begin('~/.config/nvim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'itchyny/lightline.vim'
Plug 'AlvinTaylor/tomorrow-theme', { 'rtp': 'vim' }
Plug 'townk/vim-autoclose'
Plug 'mhinz/vim-startify'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim' 
Plug 'editorconfig/editorconfig-vim'
Plug 'w0rp/ale'
Plug 'tpope/vim-surround'
call plug#end()

let g:lightline = {'colorscheme': 'wombat'}

color Tomorrow-Night
